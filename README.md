# test-pwa

Demo and test for the Progressive Web Application concepts

The purpose of this project is to demonstrate a Progressive Web Application which is multi-platform (Windows, Linux, Android, ...) and uses common web technologies.

The idea is to utilize Bootstrap and Vue for the user interface.

## Dev Run

To get a public URL, run `ngrok http 5500`.
